"""
Extra segment-manipulation functions.
"""

import re

from glue.segments import segment, segmentlist

def fromcachefilenames(filenames, coltype=int):
	"""
	Return a segmentlist describing the intervals spanned by the cachefiles
	whose names are given in the list filenames.  The segmentlist is
	constructed by parsing the file names, and the boundaries of each
	segment are coerced to type coltype.

	NOTE:  the output is a segmentlist as described by the file names;
	if the file names are not in time order, or describe overlaping
	segments, then thusly shall be the output of this function.  It is
	recommended that this function's output be coalesced before use.
	"""
	pattern = re.compile(r"-([\d.]+)-([\d.]+)\.[\w_+#\.]+\Z")
	l = segmentlist()
	for name in filenames:
		[(s, d)] = pattern.findall(name.strip())
		s = coltype(s)
		d = coltype(d)
		l.append(segment(s, s+d))
	return l

def disjoint_cover(seglist):
    """
    Build a disjoint list of segments from list of overlapping segments,
    seglist, iterating biggest to smallest.  Returns a sorted segmentlist.

    Ex:
    >>> seglist = segmentlist([segment(1, 3), segment(1, 2), segment(2, 3), segment(4, 5)])
    >>> disjoint_cover(seglist)
        segmentlist([segment(1, 3), segment(4, 5)])
    """
    seglist = segmentlist(seglist[:])   # don't mutate original
    seglist.sort(key=abs)
    uncovered = segmentlist(seglist[:]).coalesce()

    if abs(seglist) == abs(uncovered):  # already disjoint
        return seglist

    disjoint_segs = segmentlist()
    for seg in seglist[::-1]:
        if seg in uncovered:
            disjoint_segs.append(seg)
            uncovered -= segmentlist([seg])
        if abs(uncovered) == 0: break  # perfect cover
    disjoint_segs.sort()

    # sanity check: segments disjoint
    assert abs(disjoint_segs) == abs(segmentlist(disjoint_segs[:]).coalesce())

    return disjoint_segs
