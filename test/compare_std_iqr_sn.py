from numpy import std
from numpy.random.mtrand import standard_normal
from pycoh.robust_scale import sn
from pycoh.order_stats import order_stat

def iqr(x):
    """
    Inter-quartile range is robust-ish, but dead simple.
    """
    n = len(x)
    return (order_stat(x, int(n * 0.75)) - order_stat(x, int(n * 0.25))) / 1.349

print "No outliers:"
print
print "n\tstd\tiqr\tsn"
for n in (30, 100, 300, 1000, 3000, 10000):
    a_std = 0
    a_sn = 0
    a_iqr = 0
    for _ in xrange(1000):
        a = standard_normal(n)
        a_std += std(a)
        a_sn += sn(a)
        a_iqr += iqr(a)

    print "%d:\t%.4f\t%.4f\t%.4f" % (n, a_std / n, a_iqr / n, a_sn / n)
print
print "One corrupted element:"
print
print "n\tstd\tiqr\tsn"
for n in (30, 100, 300, 1000, 3000, 10000):
    a_std = 0
    a_sn = 0
    a_iqr = 0
    for _ in xrange(1000):
        a = standard_normal(n)
        a[0] = 32768
        a_std += std(a)
        a_sn += sn(a)
        a_iqr += iqr(a)

    print "%d:\t%.4f\t%.4f\t%.4f" % (n, a_std / n, a_iqr / n, a_sn / n)
print
print "(n - 1) // 4 corrupted elements:"
print
print "n\tstd\tiqr\tsn"
for n in (30, 100, 300, 1000, 3000, 10000):
    a_std = 0
    a_sn = 0
    a_iqr = 0
    for _ in xrange(1000):
        a = standard_normal(n)
        a[:(n - 1) // 4] = 32768
        a_std += std(a)
        a_sn += sn(a)
        a_iqr += iqr(a)

    print "%d:\t%.4f\t%.4f\t%.4f" % (n, a_std / n, a_iqr / n, a_sn / n)
print
print "(n - 1) // 2 corrupted elements:"
print
print "n\tstd\tiqr\tsn"
for n in (30, 100, 300, 1000, 3000, 10000):
    a_std = 0
    a_sn = 0
    a_iqr = 0
    for _ in xrange(1000):
        a = standard_normal(n)
        a[:(n - 1) // 2] = 32768
        a_std += std(a)
        a_sn += sn(a)
        a_iqr += iqr(a)

    print "%d:\t%.4f\t%.4f\t%.4f" % (n, a_std / n, a_iqr / n, a_sn / n)
