/*
    Copyright (C) 2011  Nickolas Fotopoulos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <Python.h>

#include <complex.h>
#include <math.h>
#include <stdlib.h>

#include <numpy/arrayobject.h>

static int coarse_grain_double(
    const double * restrict const in, /* input array */
    double * restrict const out,      /* output array */
    const size_t in_len,              /* length of input array */
    const size_t out_len,             /* length of output array */
    const int ratio,                  /* coarse-grain ratio */
    const size_t in_stride_bytes      /* bytes between input elements */
);
static int coarse_grain_complex(
    const complex double * restrict const in, /* input array */
    complex double * restrict const out,      /* output array */
    const size_t in_len,                      /* length of input array */
    const size_t out_len,                     /* length of output array */
    const int ratio,                          /* coarse-grain ratio */
    const size_t in_stride_bytes              /* bytes between input elements */
);

static PyObject *pycoh_coarse_grain(PyObject *self, PyObject *args);

const char coarse_grain_docstring[] =
"Sum adjacent frequency bins to coarse-grain a spectrum from one frequency \n"
"resolution to another.\n";

static int coarse_grain_double(
    const double * restrict const in, /* input array */
    double * restrict const out,      /* output array */
    const size_t in_len,              /* length of input array */
    const size_t out_len,             /* length of output array */
    const int ratio,                  /* coarse-grain ratio */
    const size_t in_stride_bytes      /* bytes between input elements */
) {
    /* compute stride in elements */
    const size_t in_stride = in_stride_bytes / sizeof(*in);
    size_t i, j;

    /* shortcut trivial cases */
    if (in_len == out_len) {
        for (i = 0; i < out_len; ++i)
            out[i] = in[i * in_stride];
        return 0;
    }
    if (in_len * in_stride < out_len) return 1;  /* cannot fine-grain */

    /* coarse-grain all but last bin */
    for (i = 0; i < out_len - 1; ++i) {
        const size_t start = i * ratio * in_stride;
        out[i] = 0.;
        for (j = 0; j < ratio; ++j)
            out[i] += in[start + j * in_stride];
    }

    /* final out bin may have less than ratio in bins */
    /* NB: in_len * in_stride is the true (element) length of in; in_len
     * is just how many strides we can take. */
    {
        const size_t start = (out_len - 1) * ratio * in_stride;
        out[out_len - 1] = 0;
        for (j = 0; j < in_len - (out_len - 1) * ratio; ++j)
            out[out_len - 1] += in[start + j * in_stride];
    }

    return 0;
}

static int coarse_grain_complex(
    const complex double * restrict const in, /* input array */
    complex double * restrict const out,      /* output array */
    const size_t in_len,                      /* length of input array */
    const size_t out_len,                     /* length of output array */
    const int ratio,                          /* coarse-grain ratio */
    const size_t in_stride_bytes              /* bytes between input elements */
) {
    /* compute stride in elements */
    const size_t in_stride = in_stride_bytes / sizeof(*in);
    size_t i, j;

    /* shortcut trivial cases */
    if (in_len == out_len) {
        for (i = 0; i < out_len; ++i)
            out[i] = in[i * in_stride];
        return 0;
    }
    if (in_len * in_stride < out_len) return 1;  /* cannot fine-grain */

    /* coarse-grain all but last bin */
    for (i = 0; i < out_len - 1; ++i) {
        const size_t start = i * ratio * in_stride;
        out[i] = 0.;
        for (j = 0; j < ratio; ++j)
            out[i] += in[start + j * in_stride];
    }

    /* final out bin may have less than ratio in bins */
    /* NB: in_len * in_stride is the true (element) length of in; in_len
     * is just how many strides we can take. */
    {
        const size_t start = (out_len - 1) * ratio * in_stride;
        out[out_len - 1] = 0;
        for (j = 0; j < in_len - (out_len - 1) * ratio; ++j)
            out[out_len - 1] += in[start + j * in_stride];
    }

    return 0;
}


/* find the numpy typenum from an array or a sequence of numbers */
static int find_typenum(PyObject *arr) {
    PyObject *temp_obj;
    int i, temp_int;
    int returnval;
    char msg[256];

    if (PyArray_Check(arr)) {
        return PyArray_ISCOMPLEX(arr) ? NPY_CDOUBLE : NPY_DOUBLE;
    }
    else if (PySequence_Check(arr) && PySequence_Length(arr) > 0) {
        returnval = NPY_DOUBLE;
        for (i = 0; i < PySequence_Length(arr); i++) {
            temp_obj = PySequence_GetItem(arr, i);
            temp_int = PyNumber_Check(temp_obj);
            /* if any are complex, all are complex */
            if (temp_int && PyComplex_Check(temp_obj)) returnval = NPY_CDOUBLE;
            Py_DECREF(temp_obj);
            /* the sequence must contain only numbers */
            if (!temp_int) goto fail;
        }
        return returnval;
    }

    fail:
    snprintf(msg, 256, "input not castable to a 1-D array");
    PyErr_SetString(PyExc_ValueError, msg);
    return -1;
}

static PyObject *pycoh_coarse_grain(PyObject *self, PyObject *args) {
    PyObject *in_arr, *out_arr, *old_in_arr;
    int ratio;
    int must_clean_up = 0;
    int typenum;
    npy_intp dims[1] = {0};
    int status;
    char msg[256];

    if (!PyArg_ParseTuple(args, "Oi", &in_arr, &ratio)) return NULL;

    typenum = find_typenum(in_arr);
    if (typenum < 0) return NULL;

    /* shortcut trivial case */
    if (ratio == 1) {
        out_arr = PyArray_FROMANY(in_arr, typenum, 1, 1, NPY_CARRAY_RO);
        return out_arr;
    }

    /* sanity checks */
    if (ratio <= 0) {
        snprintf(msg, 256, "cannot perform non-positive coarse-graining");
        PyErr_SetString(PyExc_ValueError, msg);
        return NULL;
    }

    /* need input in a contiguous memory block */
    if (!PyArray_Check(in_arr) || (PyArray_TYPE(in_arr) != typenum)) {
        old_in_arr = in_arr;
        in_arr = PyArray_FROMANY(in_arr, typenum, 1, 1, NPY_CARRAY_RO);
        if (!in_arr) {
            snprintf(msg, 256, "requested a length %ld array of type %d", PyArray_SIZE(old_in_arr) * sizeof(PyArray_TYPE(old_in_arr)), typenum);
            PyErr_SetString(PyExc_MemoryError, msg);
            return NULL;
        }
        must_clean_up = 1;
    }

    /* allocate output */
    dims[0] = (npy_intp) ceil(PyArray_SIZE(in_arr) / (double) ratio);
    out_arr = PyArray_SimpleNew(1, dims, typenum);

    /* coarse grain */
    if (PyArray_ISCOMPLEX(in_arr)) {
        status = coarse_grain_complex((complex double *) PyArray_DATA(in_arr), (complex double *) PyArray_DATA(out_arr), PyArray_SIZE(in_arr), PyArray_SIZE(out_arr), ratio, PyArray_STRIDE(in_arr, 0));
    } else {
        status = coarse_grain_double((double *) PyArray_DATA(in_arr), (double *) PyArray_DATA(out_arr), PyArray_SIZE(in_arr), PyArray_SIZE(out_arr), ratio, PyArray_STRIDE(in_arr, 0));
    }
    if (status) {
        snprintf(msg, 256, "coarse_grain failed with status %d", status);
        PyErr_SetString(PyExc_ValueError, msg);
        if (must_clean_up) { Py_DECREF(in_arr); }
        Py_DECREF(out_arr);
        return NULL;
    }

    if (must_clean_up) { Py_DECREF(in_arr); }
    return out_arr;
}

static struct PyMethodDef methods[] = {
    {"coarse_grain", pycoh_coarse_grain, METH_VARARGS, coarse_grain_docstring}
};

void init_coarse_grain(void) {
    (void) Py_InitModule3("pycoh._coarse_grain", methods, "A fast coarse_grain function\n");
    import_array();
}
