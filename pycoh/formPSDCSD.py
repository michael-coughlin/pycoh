"""
formPSDCSD back-end.
"""
from __future__ import division


import cPickle
import cStringIO as StringIO
import gzip
from itertools import izip
import sys
import tempfile
import os.path as p
try:
    import sqlite3
except ImportError:
    from pysqlite2 import dbapi2 as sqlite3

import numpy as np
from glue import lal
from glue.segments import segment, segmentlist
from glue.segmentsUtils import fromsegwizard, segmentlist_range

from pycoh import git_version, signals, frutils
from pycoh import iopycoh
from pycoh.common import *

def is_in_dir(child, parent):
    """
    Returns true if child is a child of parent directory.  Expands ~.
    """
    child = p.normpath(p.expanduser(child))
    parent = p.normpath(p.expanduser(parent))

    assert(p.exists(child))
    assert(p.isdir(parent))

    # child = child.split(p.sep) # string split
    # parent = parent.split(p.sep)

    if len(child) <= len(parent):
        return False

    return child.startswith(parent)

def remove_from_scratch(filelist, dirlist, scratchdir, verbose=False):
    """
    Remove all files in filelist and remove all empty dirs in dirlist.
    """
    filelist = [x for x in filelist if p.isfile(x)]
    dirlist = [x for x in dirlist if p.isdir(x)]
    for o in filelist + dirlist:
        assert(is_in_dir(o, scratchdir))

    for f in filelist:
        if verbose: print "Removing", f
        os.remove(f)
    for d in dirlist:
        if os.listdir(d) == []:
            if verbose: print "Removing", d
            os.rmdir(d)
        else:
            if verbose: print "Could not remove %s, as it is not empty" % d

def correct_frametype(frametype, ifo):
    """
    If the frametype has RDS_C in it (calibrated h(t) frame data), then
    prepend ifo and an underscore.  Else, just return frametype.
    """
    if "RDS_C" in frametype:
            return ifo + "_" + frametype
    return frametype

def populate_FrameCacheDict(chanlist1, chanlist2, frametype1, frametype2,
    frame_cache_dir=None, verbose=False):
    """
    Return two dictionaries of FrameCache instances, the first keyed by
    (ifo, frametype) and the second keyed by channel name. These are the
    data accessors that allow one to read data from frames.  Given
    RDS_CXX_LX as frametype, it will prepend the IFO name appropriately.
    """

    orig_frametype1 = frametype1
    orig_frametype2 = frametype2

    # First set up unique data accessors (by IFO and frametype)
    site_frametype_FrameCacheDict = {}
    for ifo in ifos_from_channels(chanlist1):
        frametype1 = correct_frametype(orig_frametype1, ifo)
        site_frametype_FrameCacheDict[(ifo[0], frametype1)] = \
            frutils.AutoqueryingFrameCache(frametype1, scratchdir=frame_cache_dir, verbose=verbose)
    for ifo in ifos_from_channels(chanlist2):
        # the second column can have same or different frametypes
        # and/or IFOs
        frametype2 = correct_frametype(orig_frametype2, ifo)
        if (ifo[0], frametype2) not in site_frametype_FrameCacheDict:
            site_frametype_FrameCacheDict[(ifo[0], frametype2)] = \
                frutils.AutoqueryingFrameCache(frametype2, scratchdir=frame_cache_dir, verbose=verbose)

    # Second, set up the mapping from chanstring to our data accessors
    FrameCacheDict = {}
    for chanstring in chanlist1:
        ifo = chanstring[:2]
        frametype1 = correct_frametype(orig_frametype1, ifo)
        FrameCacheDict[chanstring] = \
            site_frametype_FrameCacheDict[(ifo[0], frametype1)]
    for chanstring in chanlist2:
        # An interesting wrinkle is when a channel appears in both
        # channel lists with different frame types.  I choose to overwrite
        # with warning.
        ifo = chanstring[:2]
        frametype2 = correct_frametype(orig_frametype2, ifo)
        if chanstring in FrameCacheDict and \
            orig_frametype1 != orig_frametype2:
            print >>sys.stderr, "warning: %s appears in both columns with "\
                "different frame type.  Using %s." % \
                (chanstring, frametype2)
        FrameCacheDict[chanstring] = \
            site_frametype_FrameCacheDict[(ifo[0], frametype2)]
    return site_frametype_FrameCacheDict, FrameCacheDict

def compute_and_write_fft(chanstring, fftfilename, chunks, FrameCacheDict,
    opts):
    """
    Compute the FFT for chanstring during the times in chunks and write
    it to fftfilename.
    """
    starting_mem = signals.report_memory()
    fftfile = open(fftfilename, "wb")
    for i, chunk in enumerate(chunks):
        #fft = signals.fftchunk(chanstring, opts.resample_rate, chunk,
        #FrameCacheDict[chanstring], opts.highpass_order, opts.highpass_freq,
        #opts.window_type, opts.verbose)
        
        # I used this to ignore missing channels, but it was a BAD idea
        try:
            fft = signals.fftchunk(chanstring, opts.resample_rate, chunk,
            FrameCacheDict[chanstring], opts.highpass_order, opts.highpass_freq,
            opts.window_type, opts.verbose)
        except:
            #f = open("/home/nathaniel.strauss/pycoh_runs_test/failed_channels.txt","a")
            #f.write(chanstring + "\n")
            #f.close()
            continue

        cPickle.dump(fft, fftfile, 2)

        # hack to reclaim memory; do it every 100 MB; check every 100 chunks
        if (i % 100 == 0) and (signals.report_memory() - starting_mem) > 100000:
            print >>sys.stderr, "info: triggered 100MB dump to disk"
            fftfile.close()
            fftfile = open(fftfilename, "ab")

def compute_and_write_psd(chanstring, psdfilename, fftfiledict, invdf):
    """
    Compute the PSD for chanstring using the FFTs in fftfiledict and write it to
    psdfilename.
    """
    # Compute PSD
    psddict = signals.psd(fftfiledict[chanstring], invdf=invdf)

    # Add version information
    psddict["git_version_id"] = git_version.id

    # Write PSD
    iopycoh.save_dict(psdfilename, psddict)

#def compute_and_write_csd(chanstring1, chanstring2, csdfilename, fftfiledict,
#                          psd_weights=None, invdf=None, contract_weights=False, overlap_half=True):
def compute_and_write_csd(chanstring1, chanstring2, csdfilename, fftfiledict,
                          psd_weights, invdf, contract_weights, overlap_half):
    """
    Compute the CSD for the chanstrings using the FFTs in fftfiledict and
    write it to csdfilename.
    """
    # Compute CSD
    #csddict = signals.csd(fftfiledict[chanstring1], fftfiledict[chanstring2],
    #                      weight_dict=psd_weights, invdf=invdf, contract_weights=contract_weights, overlap_half=overlap_half)
    csddict = signals.csd(fftfiledict[chanstring1], fftfiledict[chanstring2],
                          psd_weights, invdf, contract_weights, overlap_half)

    # testing contents
    #f = open("/home/nathaniel.strauss/test_output.txt","w")
    #f.write("Coherences Greater than 1:\n")
    #f.write(repr(csddict))
    #for i in csddict['totppc']:
    #    if abs(i) > 1: f.write(str(abs(i)) + "\n")
    #f.close()

    # Add version information
    csddict["git_version_id"] = git_version.id

    # Write CSD
    iopycoh.save_dict(csdfilename, csddict)

def compute_and_write_x3_gamma3(chanstring1, chanstring2, xgfilename,
                                fftfiledict, invdf, overlap_half=True):
    """
    Compute Gamma3 = 1 / (PSD1 * PSD2) and X3 = CSD * Gamma3, where the PSDs
    are computed from adjacent chunks. The adjacency requirement means that
    X3 and Gamma3 are not computed for the chunks at the ends of segments.
    """
    # Compute quantities
    xgdict = signals.x3_gamma3(fftfiledict[chanstring1],
        fftfiledict[chanstring2], invdf=invdf, overlap_half=overlap_half)

    # Add version information
    xgdict["git_version_id"] = git_version.id

    # Write quantities
    iopycoh.save_dict(xgfilename, xgdict)


def main(opts, segments):
    """Write all PSDs and CSDs for the segs in segments and parameters in
    paramfilepickle."""
    df = 1 / opts.inverse_freq_resolution

    # Use mkdtemp() to create a scratch directory without possibility
    # of interference from another formPSDCSD seg running on the same machine
    scratchdir = tempfile.mkdtemp(dir=opts.scratchdir)
    if opts.cache_frames:
        frame_cache_dir = scratchdir
    else:
        frame_cache_dir = None

    # Set up access to FrameCache
    site_frametype_FrameCacheDict, FrameCacheDict = populate_FrameCacheDict(\
        opts.chanlist1, opts.chanlist2, opts.frametype1, opts.frametype2,
        frame_cache_dir, opts.verbose)

    # Create fftdir
    fftdir = "%s/fft" % scratchdir
    mkdir_secure(fftdir)

    ## Main loop
    skips = 0
    for seg in segments:
        if opts.verbose: print "%s of %s:" % (repr(seg).capitalize(), segments)

        # Set up logging
        psdlog = PSDCache()
        log_fname = log_filename(opts.logdir,
            ifos_from_channels(opts.all_channels), "FORMPSDCSD",
            segmentlist([seg]))
        if os.path.exists(log_fname):
            print >>sys.stderr, "warning: log file %s already exists; skipping" % log_fname
            skips += 1
            continue

        # If there will be no chunks to process, skip this seg
        if abs(seg) < opts.window_length_seconds:
            if opts.verbose: print >>sys.stderr, \
                "warning: segment %s is shorter than the minimum.  Skipping..."\
                % repr(seg)
            continue

        # Build up list of chunks to window and FFT
        chunk_len = opts.window_length_seconds
        chunks = segmentlist(segmentlist_range(seg[0], seg[1], chunk_len))
        if opts.overlap_half:
            chunks.extend(segmentlist(segmentlist_range(seg[0] + chunk_len//2,
                seg[1], chunk_len)))
            chunks.sort()  # strict time ordering helps cache hit ratio
        if opts.contract_weights:
            if opts.overlap_half:
                weighted_chunks = chunks[2:-2]
            else:
                weighted_chunks = chunks[1:-1]
            if len(weighted_chunks) == 0:
                raise ValueError("--contract-weights led to an empty segment")
        else:
            weighted_chunks = chunks

        # get PSD weights
        if opts.psd_weights_file is not None:
            conn = sqlite3.connect(opts.psd_weights_file)
            buffers = (conn.execute("SELECT weight_buffer FROM weights WHERE start=?;", (s[0],)).fetchone()[0] for s in weighted_chunks)
            psd_weights = dict((c, np.lib.format.read_array(StringIO.StringIO(b))) for c, b in izip(weighted_chunks, buffers))
            conn.close()
        else:
            psd_weights = None

        ## Iterate over channels and compute FFTs by calling signals.fftchunk,
        ## then form PSDs.
        # set cache files for the new chunk
        for key, val in site_frametype_FrameCacheDict.iteritems():
            ifo, frametype = key
            cache_fname = cache_filename(opts.cachedir, ifo[0], frametype, chunks)
            if os.path.exists(cache_fname):
                val.add_cache(lal.Cache.fromfile(open(cache_fname), coltype=int))

        if opts.verbose: print "Forming FFTs..."
        fftfiledict = {}
        for chanstring in opts.all_channels:
            if opts.verbose: print " ", chanstring

            fftfilename = fft_filename(fftdir, chanstring, chunks,
                                       ".pickle.gz")
            try:
                compute_and_write_fft(chanstring, fftfilename, chunks,
                                      FrameCacheDict, opts)
                fftfiledict[chanstring] = fftfilename
            except:
                print >>sys.stderr, "Exception caught for %s." % fftfilename
                print >>sys.stderr, "Cleaning up before aborting..."
                del FrameCacheDict
                remove_from_scratch(fftfiledict.values(), \
                                   [fftdir, scratchdir], \
                                   opts.scratchdir)
                raise

        # Clear most of the framefile cache (maybe leave one for next seg)
        for d in FrameCacheDict.values():
            d.unfetch(seg[0], seg[1])

        ## Form PSDs from FFTs ##
        if opts.verbose: print "Computing PSDs..."
        for chanstring in opts.all_channels:
            if opts.verbose: print " ", chanstring

            psddir = os.path.join(scratchdir, chanstring)
            psdfilename = psd_filename(psddir, chanstring, chunks,
                                       opts.output_format)
            mkdir_secure(psddir)

            # Compute PSD.  If there's an error, clean up and exit.
            try:
                compute_and_write_psd(chanstring, psdfilename, fftfiledict,
                                      opts.inverse_freq_resolution)
            except:
                print >>sys.stderr, "Exception caught for %s PSD." % chanstring
                print >>sys.stderr, "Cleaning up before aborting..."
                del FrameCacheDict
                remove_from_scratch(fftfiledict.values(), \
                                    [fftdir, scratchdir], \
                                    opts.scratchdir)
                raise
            psdlog.add_psd(psdfilename)

        ## Iterate over channel pairs and multiply FFTs to form CSDs ##
        if opts.verbose: print "Computing CSDs..."
        for chanpair,chanstring in izip(opts.chanpairs, opts.chanstrings):
            if opts.verbose: print " ", chanstring

            csddir = os.path.join(scratchdir, chanstring)
            csdfilename = csd_filename(csddir, chanpair, chunks,
                                       opts.output_format)
            mkdir_secure(csddir)

            # Compute and write CSD.  If there's an error, clean up and exit.
            try:
                compute_and_write_csd(chanpair[0], chanpair[1], csdfilename,
                                      fftfiledict, psd_weights,
                                      opts.inverse_freq_resolution,
                                      opts.contract_weights, opts.overlap_half)
            except:
                print >>sys.stderr, "Exception caught for %s CSD." \
                    % chanstring
                print >>sys.stderr, "Cleaning up before aborting..."
                del FrameCacheDict
                remove_from_scratch(fftfiledict.values(), \
                                    [fftdir, scratchdir], \
                                    opts.scratchdir)
                raise

            # Write
            psdlog.add_psd(csdfilename)

        ## Iterate over channel pairs and multiply FFTs to form X3 and Gamma3 ##
        if opts.verbose: print "Computing X3 and Gamma3..."
        for chanpair,chanstring in izip(opts.chanpairs, opts.chanstrings):
            if opts.verbose: print " ", chanstring

            x3g3dir = os.path.join(scratchdir, chanstring)
            x3g3filename = x3g3_filename(x3g3dir, chanpair, chunks,
                opts.output_format)
            mkdir_secure(x3g3dir)

            # Compute and write X3 and Gamma3.
            # If there's an error, clean up and exit.
            try:
                compute_and_write_x3_gamma3(chanpair[0], chanpair[1],
                    x3g3filename, fftfiledict, opts.inverse_freq_resolution,
                    opts.overlap_half)
            except:
                print >>sys.stderr, "Exception caught for %s X3 and Gamma3." \
                    % chanstring
                print >>sys.stderr, "Cleaning up before aborting..."
                del FrameCacheDict
                remove_from_scratch(fftfiledict.values(), \
                                    [fftdir, scratchdir], \
                                    opts.scratchdir)
                raise

            # Write
            psdlog.add_psd(x3g3filename)

        ## Assuming we got here, there have been no errors.
        ## Clean FFTs, but leave PSDs!
        if not opts.keep_ffts:
            remove_from_scratch(fftfiledict.values(), [], opts.scratchdir)
        psdlog.tofile(open(log_fname, "w"))

    if skips == len(segments):
        print >>sys.stderr, "warning: skipped all log files"

    del site_frametype_FrameCacheDict, FrameCacheDict
    remove_from_scratch([], [fftdir, scratchdir], opts.scratchdir)
    return True
