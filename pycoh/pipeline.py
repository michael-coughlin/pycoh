"""
Subclasses of glue.pipeline's Condor DAG-creation classes.
"""

from glue import pipeline
from glue.segments import segmentlist

from pycoh.common import log_filename, veto_filename

##############################################################################
# Define Jobs.  A Job corresponds to a program.  Each Job can have
# multiple Nodes (instances).
##############################################################################

class FormPSDCSDJob(pipeline.CondorDAGJob):
    """
    This class represents the formPSDCSD program.  Static parameters common
    to all formPSDCSD Nodes are stored here.  These are read from the
    [formPSDCSD] section of the ini file.
    """
    def __init__(self, cp, rootdir=""):
        """
        cp = a ConfigParser instance
        rootdir = the root directory of the analysis
        """
        self.__executable = cp.get('condor','formpsdcsd-bin')
        self.__universe = "vanilla"
        pipeline.CondorDAGJob.__init__(self,self.__universe, self.__executable)

        self.set_stdout_file('logs/formPSDCSD-$(cluster)-$(process).out')
        self.set_stderr_file('logs/formPSDCSD-$(cluster)-$(process).err')
        self.add_condor_cmd('getenv','True')
        self.add_condor_cmd('accounting_group',cp.get("condor", "accounting_group"))
        # set directories
        self.add_opt("scratchdir", cp.get("pipeline", "scratchdir"))

        # set frametype
        self.add_opt("frametype1", cp.get("channels", "frametype1"))
        self.add_opt("frametype2", cp.get("channels", "frametype2"))

        # the straight pass-through doesn't work for booleans
        if cp.getboolean("data-conditioning", "overlap-half"):
            self.add_opt("overlap-half", "")
        cp.remove_option("data-conditioning", "overlap-half")

        # formPSDCSD needs most of the information in the paramfile
        self.add_ini_opts(cp, "channels")
        self.add_ini_opts(cp, "output")
        self.add_ini_opts(cp, "data-conditioning")

        # add anything extra from the [formPSDCSD] section of ini file
        self.add_ini_opts(cp, "formPSDCSD")

        # set soft kill to increase the chance we'll gracefully clean up
        self.add_condor_cmd("remove_kill_sig", "SIGUSR2")

    def set_logdir(self, logdir):
        self.add_opt("logdir", logdir)

    def set_ifos(self, ifos):
        self._ifos = "".join(ifos)  # store for file-naming, but don't pass as opt

class HarvestJob(pipeline.CondorDAGJob, pipeline.AnalysisJob):
    """
    This class represents the harvest program.  Static parameters common
    to all harvest Nodes are stored here.  These are read from the
    [harvest] section of the ini file.
    """
    def __init__(self, cp, rootdir=""):
        """
        cp = a ConfigParser instance
        rootdir = the root directory of the analysis
        """
        self.__executable = cp.get('condor','harvest-bin')
        self.__universe = "vanilla"
        pipeline.CondorDAGJob.__init__(self,self.__universe, self.__executable)

        self.set_stdout_file('logs/harvest-$(cluster)-$(process).out')
        self.set_stderr_file('logs/harvest-$(cluster)-$(process).err')
        self.add_condor_cmd('getenv','True')
        self.add_condor_cmd('accounting_group',cp.get("condor", "accounting_group"))

        # formPSDCSD needs most of the information in the paramfile
        self.add_ini_opts(cp, "output")

        # add anything extra from the [harvest] section of ini file
        self.add_ini_opts(cp, "harvest")

    def set_logdir(self, logdir):
        self.add_opt("logdir", logdir)

    def set_outdir(self, outdir):
        self.add_opt("outdir", outdir)

    def set_ifos(self, ifos):
        self._ifos = "".join(ifos)  # store for file-naming, but don't pass as opt

class IdentifyOutliersJob(pipeline.CondorDAGJob, pipeline.AnalysisJob):
    """
    This class represents the identify_outliers program.  Static parameters
    common to all identify_outliers Nodes are stored here.  These are read from
    the [identify_outliers] section of the ini file.
    """
    def __init__(self, cp, rootdir=""):
        """
        cp = a ConfigParser instance
        rootdir = the root directory of the analysis
        """
        self.__executable = cp.get('condor','identify_outliers-bin')
        self.__universe = "vanilla"
        pipeline.CondorDAGJob.__init__(self,self.__universe, self.__executable)

        self.set_stdout_file('logs/identify_outliers-$(cluster)-$(process).out')
        self.set_stderr_file('logs/identify_outliers-$(cluster)-$(process).err')
        self.add_condor_cmd('getenv','True')
        self.add_condor_cmd('accounting_group',cp.get("condor", "accounting_group"))

        # add anything extra from the [harvest] section of ini file
        self.add_ini_opts(cp, "identify_outliers")

    def set_logdir(self, logdir):
        self._logdir = logdir  # store for file-naming, but don't pass as opt

    def set_ifos(self, ifos):
        self._ifos = "".join(ifos)  # store for file-naming, but don't pass as opt

##############################################################################
# Define Nodes.  A Node corresponds to a single instance of a program to be
# run.  They each attach to a Job, which contains the information common to
# all Nodes of a single type.
##############################################################################

class FormPSDCSDNode(pipeline.CondorDAGNode):
    def __init__(self, job):
        """
        A FormPSDCSDNode runs an instance of formPSDCSD in a Condor DAG.
        """
        pipeline.CondorDAGNode.__init__(self, job)
        self.segments = []

    def add_segment(self, seg):
        job = self.job()
        self.add_var_arg("%d:%d" % seg)
        self.add_output_file(log_filename(job.get_opts()["logdir"],
            job._ifos, "FORMPSDCSD", segmentlist([seg])))
        self.segments.append(seg)

class HarvestNode(pipeline.CondorDAGNode):
    def __init__(self, job, window_seg):
        """
        A HarvestNode runs an instance of harvest in a Condor DAG.
        """
        pipeline.CondorDAGNode.__init__(self, job)

        opts = job.get_opts()
        self.add_output_file(log_filename(opts["logdir"],
            job._ifos, "HARVEST", segmentlist([window_seg])))

class IdentifyOutliersNode(pipeline.CondorDAGNode):
    def __init__(self, job, seg):
        """
        A HarvestNode runs an instance of harvest in a Condor DAG.
        """
        pipeline.CondorDAGNode.__init__(self, job)

        outfname = veto_filename(job._logdir,
            job._ifos, "OUTLIERS_VETO", segmentlist([seg]))
        self.add_var_opt("output", outfname)
        self.add_output_file(outfname)
