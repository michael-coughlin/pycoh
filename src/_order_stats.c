/*
 *  R : A Computer Language for Statistical Data Analysis
 *  Copyright (C) 1995, 1996  Robert Gentleman and Ross Ihaka
 *  Copyright (C) 1998-2012   The R Core Team
 *  Copyright (C) 2004        The R Foundation
 *  Copyright (C) 2012        Nickolas Fotopoulos
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, a copy is available at
 *  http://www.r-project.org/Licenses/
 */

#include <stdlib.h>
#include <math.h>
#include <string.h>


/* R-defined extra careful cmp function */
static int rcmp(double x, double y, int nalast)
{
    int nax = isnan(x), nay = isnan(y);
    if (nax && nay) return 0;
    if (nax)        return nalast?1:-1;
    if (nay)        return nalast?-1:1;
    if (x < y)      return -1;
    if (x > y)      return 1;
    return 0;
}

/*
 * Partial sort (taken from R) so that x[k] is in the correct place,
 * smaller to left, larger to right. This algorithm scrambles input.
 *
 * NOTA BENE:  k < n  required, and *not* checked here;
 *             -----  infinite loop possible otherwise!
 */
void partial_sort(double *x, const size_t n, const size_t k) {
    static int nalast = 1;
    ssize_t L, R;

    for (L = 0, R = n - 1; L < R; ) {
        const double v = x[k];
        ssize_t i = L, j = R;
        for(; i <= j;) {
            while (rcmp(x[i], v, nalast) < 0) i++;
            while (rcmp(v, x[j], nalast) < 0) j--;
            if (i <= j) {
                const double w = x[i];
                x[i++] = x[j];
                x[j--] = w;
            }
        }
        if (j < k) L = i;
        if (k < i) R = j;
    }
}

/*
 * Finds the k-th order statistic of an array a[] of length n
 *
 * A partial sort is required, which scrambles the input array,
 * so define the straightforward destructive version and a wrapper
 * that performs a copy.
 */
double destructive_order_stat(double *a, const size_t n, const size_t k) {
    const size_t ind = k - 1; /* 0-indexing */
    partial_sort(a, n, ind); /* partial sort */
    return a[ind];
}

double order_stat(const double *a_in, const size_t n, const size_t k) {
    double *a = malloc(n * sizeof(double));
    memcpy(a, a_in, n * sizeof(double));
    double ax = destructive_order_stat(a, n, k);
    free(a);
    return ax;
}
