"""
Signal-processing library
"""

from __future__ import division
from resource import getrusage, RUSAGE_SELF
from itertools import izip, imap
from warnings import warn

import numpy
from numpy.fft import rfft
from scipy import flipud
from scipy import signal

from pycoh.metaarray import Spectrum
import lal as lalconstants
#from pylal import lalconstants
import os
print os.path.abspath(lalconstants.__file__) > "LOOKFORMEHERE"

from pycoh import iopycoh
from pycoh._coarse_grain import coarse_grain  # C implementation

__author__ = "Nickolas Fotopoulos <nickolas.fotopoulos@ligo.org>"

def report_memory():
    return getrusage(RUSAGE_SELF).ru_maxrss

def filtfilt(b, a, input_vector):
    """
    Filter forwards, then backwards to achieve exactly zero phase delay.
    input_vector has shape (n,1).
    """
    forward = signal.lfilter(b, a, input_vector, axis=0)
    return flipud(signal.lfilter(b, a, flipud(forward), axis = 0))

_freqz_cache = {}
def memoized_freqz(b, a, n):
    b = tuple(b)  # ndarray not hashable; tuple works fine with freqz
    a = tuple(a)  # ndarray not hashable; tuple works fine with freqz
    if (b, a, n) not in _freqz_cache:
        return _freqz_cache.setdefault((b, a, n), signal.freqz(b, a, n))
    return _freqz_cache[b, a, n]

def filtfilt_fd(b, a, input_fd):
    """
    The frequency-domain analogue of filtfilt.
    """
    w, h = memoized_freqz(b, a, len(input_fd))
    h = h[:len(input_fd)]  # freqz bug: sometimes contains extra point (pi)
    return (h.conj() * h).real * input_fd

_butter_cache = {}
def memoized_butter(*args):
    """
    Return the output of scipy.signal.butter, but cache results.
    Filter design turns out to be costly due to the combinatorics.
    """
    if args not in _butter_cache:
        _butter_cache[args] = signal.butter(*args)
    return _butter_cache[args]

_cheby1_cache = {}
def memoized_cheby1(*args):
    """
    Return the output of scipy.signal.cheby1, but cache results.
    Filter design turns out to be costly due to the combinatorics.
    """
    if args not in _cheby1_cache:
        _cheby1_cache[args] = signal.cheby1(*args)
    return _cheby1_cache[args]

def decimate(data, R, N=8):
    """
    Decimate data by a factor of R with an Nth order Chebyshev Type 1
    low-pass filter.  Apply filter with filtfilt to avoid introducing a
    phase delay.
    """
    # Get filter coefficients for 0.05dB ripple and lowpass freq = 0.8*desired
    # output sampling frequency. 8th order Chebyshev.
    b, a = memoized_cheby1(N, 0.05, 0.8/R)

    # Filter forwards and backwards
    newdata = filtfilt(b, a, data)

    # Return every Rth sample.
    return newdata[::R]

def decimate_fd(data_fd, R, N=8):
    """
    The frequency-domain analogue of decimate. Low-pass filter, then truncate
    the high-frequency components.
    """
    b, a = memoized_cheby1(N, 0.05, 0.8 / R)
    filtfilt_fd(b, a, data_fd)
    return data_fd[:int(len(data_fd) / R)]

def has_consecutive_zeros(arr):
    """
    Detect bad DAQ by looking for 20 or more consecutive zeros in
    the data stream.
    """
    if (arr==0).any():
        zz = (arr==0)
        z0 = zz
        for ii in range(1,20): # 20 or more consecutive zeros
            z0 = z0 & numpy.roll(zz, ii)
        zeros = z0.any()
    else:
        zeros = False
    return zeros

def has_consecutive_saturations(arr):
    """
    Detect bad DAQ by looking for two or more consecutive values
    over 32000 in the data stream.
    """
    saturations = (abs(arr) > 32000)
    return (saturations[1:] & saturations[:-1]).any()

def fftchunk(chan, maxsamplerate, chunk, myData,
    highpass_order=None, highpass_freq=None, window_name="hanning",
    verbose=False):
    """
    Return a decimated (down-sampled), highpass-filtered, detrended,
    windowed FFT of the data for the given channel.
    """
    if (highpass_order is None) ^ (highpass_freq is None):
        raise ValueError("must specify both highpass_order and " \
            "highpass_freq or neither.")

    # Read data into TimeSeries
    v = myData.fetch(chan, chunk[0], chunk[1])
    v_meta = v.metadata
    v = v.A

    # Report errors
    if len(v) == 0:
        raise IOError("No data returned for %s in chunk %s." % (chan, str(chunk)))
    
    # This bit of code causes coherences of greater than one when only one channel
    # has missing segments
    #if has_consecutive_zeros(v):
    #    warn("Data contains two or more consecutive zeros. Skipping.")
    #    return None
    #if has_consecutive_saturations(v):
    #    warn("Data contains two or more consecutive saturations. Skipping.")
    #    return None

    # High-pass filter
    f_Ny = 0.5 / v_meta.dt
    if highpass_order is not None:
        b, a = memoized_butter(highpass_order, highpass_freq / f_Ny, "high")
        v = filtfilt(b, a, v)

    # Decimate -- this will fail if frequencies are not commensurate!!
    N = min(len(v), maxsamplerate * abs(chunk))
    if N != len(v):
        v = decimate(v, len(v)//N)
    samplerate = N / abs(chunk)

    # Set window with normalization such that the PSD satisfies
    # Parseval's Theorem (power is conserved)
    try:
        window_func = getattr(signal, window_name)
    except AttributeError:
        raise ValueError("invalid window type: " + window_name)
    window = window_func(N)
    window /= numpy.sqrt((window**2).sum() * samplerate)

    # coalesce segments to save disk space; too much metadata!
    v_meta.segments.coalesce()

    # set metadata for new Spectrum
    meta = {"name": "%s_FFT" % v_meta.name, "df": 1 / abs(chunk), "f_low": 0,
        "segments": v_meta.segments, "comments": v_meta.comments}

    # Detrend, window, and compute fft.  Take only positive frequencies.
    # Store in single precision to reduce disk usage.
    return Spectrum(rfft(window*signal.detrend(v)).astype('c8'), meta)

def psd(fftfilename, invdf=None):
    """
    Given the filenames of files with FFTs, compute the PSDs, and
    return a dictionary containing the PSD, the number of chunks
    averaged, and the sum of the weighting factors used.
    """
    # set up FFTs
    ffts = iopycoh.pickle_iterator(open(fftfilename))

    # Compute PSD
    nchunks = 0
    totPSD = 0          # additive identity, broadcastable to arb. shape
    totPSD_meta = None  # metadata identity under |
    for fft in ffts:
        if fft is None: continue  # can occur when, e.g., DAQ is stuck
        totPSD += (fft.H.A.astype(numpy.complex128) * \
                   fft.A.astype(numpy.complex128)).real
        totPSD_meta |= fft.metadata
        nchunks += 1
    if invdf is not None and totPSD_meta is not None:
        orig_df = totPSD_meta.df
        cg_fac = int(1. / (orig_df * invdf))
        totPSD = coarse_grain(totPSD, cg_fac)
        totPSD_meta.df = 1. / invdf
        nchunks *= cg_fac  # coarse-graining adds multiple bins
    totPSD = Spectrum(totPSD, totPSD_meta)

    # sanity check: there is at least one PSD (from at least one FFT)
    if not nchunks: return {"nchunks": 0, "totppc": 0., "totweight": 0.}

    segments = totPSD_meta.segments

    # determine the ratio of the number of windows to number w/o overlap
    overcoverage_fac = float(sum(imap(abs, segments))) / \
        float(abs(segments.extent()))
    totPSD /= overcoverage_fac

    # One-sided PSD; multiply by 2 to conserve power except at DC and Nyquist
    totPSD[1:-1] *= 2

    # Fix up metadata
    totPSD.metadata.df = 1. / invdf
    totPSD.metadata.name = totPSD.metadata.name.replace("FFT", "PSD")
    totPSD.metadata.segments.coalesce()

    return {'nchunks': nchunks, 'totppc': totPSD, 'totweight': nchunks}

def _dropfirstlast(it):
    """
    Wrap an iterator, passing all of its elements normally except for the
    first and last.
    """
    it.next()  # drop first
    old = it.next()
    while 1:
        new = it.next()  # when next raises StopIteration, we terminate
        yield old
        old = new

def _dropfirstlasttwo(it):
    """
    Wrap an iterator, passing all of its elements normally except for the
    first two and last two.
    """
    it.next()  # drop first
    it.next()  # drop second
    older = it.next()
    old = it.next()
    while 1:
        new = it.next()  # when next raises StopIteration, we terminate
        yield older
        older = old
        old = new

def csd(fftfilename1, fftfilename2, weight_dict=None, invdf=None, contract_weights=False, overlap_half=True):
    """
    Given the filenames of files with FFTs, compute the CSD, and
    return a dictionary containing the CSD, the number of chunks
    averaged, and the sum of the weighting factors used.

    weight_dict :  a dictionary mapping segments to a scalar or array
                   to be used for periodogram weighting
    invdf :        an integer. If provided, coarse-grain each periodogram
                   to df = 1 / invdf.
    contract_weights : if True, discard the first and last FFTs
    """
    # set up FFTs
    ffts1 = iopycoh.pickle_iterator(open(fftfilename1))
    ffts2 = iopycoh.pickle_iterator(open(fftfilename2))

    # handle the fact that stochastic.m doesn't compute CSDs or weights for
    # the first and last chunks of a segment
    if contract_weights:
        if overlap_half:
            ffts1 = _dropfirstlasttwo(ffts1)
            ffts2 = _dropfirstlasttwo(ffts2)
        else:
            ffts1 = _dropfirstlast(ffts1)
            ffts2 = _dropfirstlast(ffts2)

    # set up a default for PSD weights
    if weight_dict is None:
        class unity_dict(object):
            one = numpy.array([1.])  # broadcasts to any length
            def __getitem__(self, key):
                return self.one
        weight_dict = unity_dict()

    # Compute CSD
    cg_fac = 1          # coarse-grain factor
    nchunks = 0
    totweight = 0
    totPSD = 0          # additive identity, broadcastable to arb. shape
    totPSD_meta = None  # metadata identity under |
    totPSD_meta_b = None  # need metadata from b for naming purposes only
    for a, b, w in izip(ffts1, ffts2, weight_dict):
        if a is None or b is None: continue  # can occur when, e.g., DAQ is stuck

        # sanity check: a and b represent the same segment
        assert len(a.metadata.segments) == 1
        assert len(b.metadata.segments) == 1
        #assert a.metadata.segments == b.metadata.segments
        if not a.metadata.segments == b.metadata.segments:
            continue

        # sanity check: a and b are comparable spectra
        assert a.metadata.df == b.metadata.df
        assert a.metadata.f_low == b.metadata.f_low

        # extract weight
        seg = a.metadata.segments[0]
        try:
            w = weight_dict[seg]
        except KeyError:
            raise KeyError("warning: no weight found for " + str(seg))
        if w.sum() == 0.:
            continue  # do not merge into metadata either

        # match spectrum lengths, low-pass filtering if necessary
        min_len = min(len(a), len(b))
        if len(a) > min_len:
            a = decimate_fd(a, len(a) / min_len)
        if len(b) > min_len:
            b = decimate_fd(b, len(b) / min_len)

        periodogram = (a.H.A.astype(numpy.complex128) * \
                       b.A.astype(numpy.complex128))
        cg_fac = 1
        if invdf is not None:
            cg_fac = int(1. / (a.metadata.df * invdf))  # coarse-graining adds multiple bins
            periodogram = coarse_grain(periodogram, cg_fac)
        totPSD += periodogram * w[:len(periodogram)]
        totPSD_meta |= a.metadata
        totPSD_meta_b = b.metadata
        nchunks += cg_fac
        totweight += w[:len(periodogram)] * cg_fac
    # sanity check: there is at least one PSD (from at least one FFT)
    if not nchunks: return {"nchunks": 0, "totppc": 0., "totweight": 0.}

    totPSD_meta.df = 1. / invdf
    totPSD = Spectrum(totPSD, totPSD_meta)
    segments = totPSD_meta.segments

    # determine the ratio of the number of windows to number w/o overlap
    overcoverage_fac = float(sum(imap(abs, segments))) / \
        float(abs(segments.extent()))
    totPSD /= overcoverage_fac

    # One-sided PSD; multiply by 2 to conserve power except at DC and Nyquist
    totPSD[1:-1] *= 2

    # Fix up metadata
    totPSD.metadata.df = 1. / invdf
    totPSD.metadata.name = totPSD.metadata.name.replace("FFT",
        totPSD_meta_b.name.replace("FFT", "CSD"))
    totPSD.metadata.segments.coalesce()
    
    #f = open("/home/nathaniel.strauss/coh1test.txt",'w')
    #for i in totPSD:
    #    try:
    #       print i
    #       # if abs(i) > 1:
    #       #     f.write(str(abs(i)) + "\n")
    #    except:
    #        pass
    #f.close()
    
    return {'nchunks': nchunks, 'totppc': totPSD, 'totweight': totweight}

#const_fac = 10 * lalconstants.LAL_PI**2 / (3 * lalconstants.LAL_H0FAC_SI**2)
const_fac = 10 * lalconstants.PI**2 / (3 * lalconstants.H0FAC_SI**2)
def x3_gamma3(fftfilename1, fftfilename2, invdf, window_name="hann", overlap_half=True):
    """
    Given the filenames of files with FFTs, compute the CSD, and return a
    dictionary containing Gamma3 = 1 / (PSD1 * PSD2) and X3 = CSD * Gamma3,
    where the PSDs are computed from adjacent chunks. The adjacency requirement
    means that X3 and Gamma3 are not computed for the chunks at the ends of
    segments.
    """
    df = 1. / invdf

    # set up FFTs
    ffts1 = iopycoh.pickle_iterator(open(fftfilename1))
    ffts2 = iopycoh.pickle_iterator(open(fftfilename2))

    # Compute PSDs and CSD
    nchunks = 0
    totX3 = 0          # additive identity, broadcastable to arb. shape
    totGamma3 = 0          # additive identity, broadcastable to arb. shape
    if overlap_half:
        neighbor = 2  # adjacent disjoint chunk is two indices away
    else:
        neighbor = 1  # adjacent disjoint chunk is one index away
    oldPSD1 = (None,) * (2 * neighbor)  # i - 4, i - 3, i - 2, i - 1
    oldPSD2 = (None,) * (2 * neighbor)  # i - 4, i - 3, i - 2, i - 1
    oldCSD = (None,) * neighbor  # i - 2, i - 1
    meta = None  # metadata identity under |
    meta_b = None  # need metadata from b for naming purposes only
    for a, b in izip(ffts1, ffts2):
        if a is None or b is None: continue  # can occur when, e.g., DAQ is stuck

        # sanity check: a and b represent the same segment
        assert len(a.metadata.segments) == 1
        assert len(b.metadata.segments) == 1
        #assert a.metadata.segments == b.metadata.segments
        if not a.metadata.segments == b.metadata.segments:
            continue

        # sanity check: a and b are comparable spectra
        orig_df = a.metadata.df
        assert orig_df == b.metadata.df
        assert a.metadata.f_low == b.metadata.f_low

        # match spectrum lengths, low-pass filtering if necessary
        min_len = min(len(a), len(b))
        if len(a) > min_len:
            a = decimate_fd(a, len(a) / min_len)
        if len(b) > min_len:
            b = decimate_fd(b, len(b) / min_len)

        # make sure we have enough precision for O(h(t)^2)
        a = a.astype(numpy.complex128)
        b = b.astype(numpy.complex128)

        # compute current PSDs
        curPSD1 = (a.H.A * a.A).real
        curPSD2 = (b.H.A * b.A).real
        curCSD = a.H.A * b.A

        # one-sided PSD: multiply by 2 except at DC and Nyquist
        curPSD1[1:-1] *= 2
        curPSD2[1:-1] *= 2
        curCSD[1:-1] *= 2

        # coarse-grain
        cg_fac = int(1. / (orig_df * invdf))
        curPSD1 = coarse_grain(curPSD1, cg_fac)
        curPSD2 = coarse_grain(curPSD2, cg_fac)
        curCSD = coarse_grain(curCSD, cg_fac)

        # contribute to Gamma3 and X3
        # Note that we are assigning the (i-2)-th element based on i-4 and i.
        if oldPSD1[0] is not None:
            curGamma3 = 4. / ((oldPSD1[0] + curPSD1) * (oldPSD2[0] + curPSD2))
            totGamma3 += curGamma3
            totX3 += oldCSD[0] * curGamma3  # CSD from (i-2)-th element
            nchunks += cg_fac  # coarse-graining adds multiple bins

        # update history
        oldPSD1 = oldPSD1[1:] + (curPSD1,)
        oldPSD2 = oldPSD2[1:] + (curPSD2,)
        oldCSD = oldCSD[1:] + (curCSD,)

        meta |= a.metadata
        meta_b = b.metadata

    # sanity check: there is at least one PSD (from at least one FFT)
    if not nchunks: return {"nchunks": 0, "totppc": 0., "totweight": 0.}

    # determine window factors
    try:
        window_func = getattr(signal, window_name)
    except AttributeError:
        raise ValueError("invalid window type: " + window_name)
    window = window_func(min_len)
    w1w2bar = (window**2).mean()
    w1w2squaredbar = (window**4).mean()

    # make an attempt at normalization
    # NB: I am only guessing at normalization; only take relative values
    #     seriously.
    #totGamma3 /= 0.25 * const_fac * const_fac * w1w2squaredbar * abs(a.metadata.segments)  # matches John's windowing note
    #totGamma3 /= const_fac * const_fac * w1w2squaredbar  # matches Joe's note
    totGamma3 /= 4 * const_fac * const_fac * w1w2squaredbar * (0.5 * nchunks * min_len) # matches stochastic.m's output
    totX3 /= const_fac * w1w2bar * (0.5 * nchunks * min_len)

    # fix up and attach metadata
    meta.segments.coalesce()
    meta.df = df
    totGamma3 = Spectrum(totGamma3, meta)
    totX3 = Spectrum(totX3, meta.copy())
    totGamma3.metadata.name = totGamma3.metadata.name.replace("FFT",
        meta_b.name.replace("FFT", "Gamma3"))
    totX3.metadata.name = totX3.metadata.name.replace("FFT",
        meta_b.name.replace("FFT", "X3"))

    return {'nchunks': nchunks, 'totppc': totX3, "totweight": totGamma3}
