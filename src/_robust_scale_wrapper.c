/*
    Copyright (C) 2012  Nickolas Fotopoulos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <Python.h>

#include <numpy/arrayobject.h>

#include "order_stats.h"
#include "robust_scale.h"

static PyObject *pycoh_sn(PyObject *self, PyObject *args);

static const char sn_docstring[] =
"The Sn robust scale estimator of Rousseeuw, P.J. and Croux, C. (1993).\n";

static PyObject *pycoh_sn(PyObject *self, PyObject *args) {
    PyObject *in_arr, *old_in_arr;
    int is_sorted = 0, finite_corr = 1;
    int must_clean_up = 0;
    double result;
    char msg[256];

    if (!PyArg_ParseTuple(args, "O|ii:sn", &in_arr, &is_sorted, &finite_corr)) return NULL;
    Py_INCREF(in_arr);

    /* need input in a contiguous memory block */
    if (!PyArray_Check(in_arr) || (PyArray_TYPE(in_arr) != NPY_DOUBLE)) {
        old_in_arr = in_arr;
        in_arr = PyArray_FROMANY(in_arr, NPY_DOUBLE, 1, 1, NPY_CARRAY_RO);
        if (!in_arr) {
            snprintf(msg, 256, "requested a length %zd array of type %d", PyArray_SIZE(old_in_arr) * sizeof(PyArray_TYPE(old_in_arr)), NPY_DOUBLE);
            PyErr_SetString(PyExc_MemoryError, msg);
            Py_DECREF(old_in_arr);
            return NULL;
        }
        must_clean_up = 1;
    }

    /* compute */
    result = sn((double *) PyArray_DATA(in_arr), PyArray_SIZE(in_arr), is_sorted, finite_corr);
    if (must_clean_up) { Py_DECREF(old_in_arr); }
    Py_DECREF(in_arr);
    return Py_BuildValue("d", result);
}

static struct PyMethodDef methods[] = {
    {"sn", pycoh_sn, METH_VARARGS, sn_docstring},
    {NULL, NULL, 0, NULL}  /* sentinel */
};

PyMODINIT_FUNC initrobust_scale(void) {
    (void) Py_InitModule3("pycoh.robust_scale", methods, "robust scale estimation\n");
    import_array();
}
