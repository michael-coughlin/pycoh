/*
    Copyright (C) 2012  Nickolas Fotopoulos

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <Python.h>

#include <numpy/arrayobject.h>

#include "order_stats.h"

static PyObject *pycoh_partial_sort(PyObject *self, PyObject *args);
static PyObject *pycoh_order_stat(PyObject *self, PyObject *args);

const char partial_sort_docstring[] =
"Perform a partial sort on given array, guaranteeing that items before "
"array[i] <= array[k] < array[j] for i < k < j. Performed in O(n) time.\n";

static PyObject *pycoh_partial_sort(PyObject *self, PyObject *args) {
    PyObject *in_arr, *old_in_arr;
    Py_ssize_t k = -1;  /* which index to partially sort */
    int allocated_new = 0;
    char msg[256];

    if (!PyArg_ParseTuple(args, "On:partial_sort", &in_arr, &k)) return NULL;

    if (k <= 0) {
        PyErr_SetString(PyExc_ValueError, "k must be positive");
        return NULL;
    }

    Py_INCREF(in_arr);

    /* need input in a contiguous memory block */
    if (!PyArray_Check(in_arr) || (PyArray_TYPE(in_arr) != NPY_DOUBLE)) {
        old_in_arr = in_arr;
        in_arr = PyArray_FROMANY(in_arr, NPY_DOUBLE, 1, 1, NPY_CARRAY_RO);
        if (!in_arr) {
            snprintf(msg, 256, "requested a length %ld array of type %d", PyArray_SIZE(old_in_arr) * sizeof(PyArray_TYPE(old_in_arr)), NPY_DOUBLE);
            PyErr_SetString(PyExc_MemoryError, msg);
            Py_DECREF(old_in_arr);
            return NULL;
        }
        allocated_new = 1;
    }

    if (k > PyArray_SIZE(in_arr)) {
        PyErr_SetString(PyExc_ValueError, "k must be less than length of array");
        if (allocated_new) Py_DECREF(old_in_arr);
        Py_DECREF(in_arr);
        return NULL;
    }

    /* compute */
    partial_sort((double *) PyArray_DATA(in_arr), PyArray_SIZE(in_arr), k);
    if (allocated_new) Py_DECREF(old_in_arr); /* throw out old and return new */
    return Py_BuildValue("N", in_arr);
}

const char order_stat_docstring[] =
"Find the kth-order statistic of the given array in O(n) time. Equivalent "
"to but faster than sorted(array)[k - 1].\n";

static PyObject *pycoh_order_stat(PyObject *self, PyObject *args) {
    PyObject *in_arr, *old_in_arr;
    Py_ssize_t k = -1;
    int allocated_new = 0;
    double result;
    char msg[256];

    if (!PyArg_ParseTuple(args, "On:order_stat", &in_arr, &k)) return NULL;

    if (k <= 0) {
        PyErr_SetString(PyExc_ValueError, "k must be positive");
        return NULL;
    }

    Py_INCREF(in_arr);

    /* need input in a contiguous memory block */
    if (!PyArray_Check(in_arr) || (PyArray_TYPE(in_arr) != NPY_DOUBLE)) {
        old_in_arr = in_arr;
        in_arr = PyArray_FROMANY(in_arr, NPY_DOUBLE, 1, 1, NPY_CARRAY_RO);
        if (!in_arr) {
            snprintf(msg, 256, "requested a length %ld array of type %d", PyArray_SIZE(old_in_arr) * sizeof(PyArray_TYPE(old_in_arr)), NPY_DOUBLE);
            PyErr_SetString(PyExc_MemoryError, msg);
            Py_DECREF(old_in_arr);
            return NULL;
        }
        allocated_new = 1;
    }
    if (k > PyArray_SIZE(in_arr)) {
        PyErr_SetString(PyExc_ValueError, "k must be less than length of array");
        if (allocated_new) Py_DECREF(old_in_arr);
        Py_DECREF(in_arr);
        return NULL;
    }

    /* compute */
    if (allocated_new)
        result = destructive_order_stat((double *) PyArray_DATA(in_arr), (size_t) PyArray_SIZE(in_arr), (size_t) k);
    else
        result = order_stat((double *) PyArray_DATA(in_arr), (size_t) PyArray_SIZE(in_arr), (size_t) k);
    if (allocated_new) { Py_DECREF(old_in_arr); }
    Py_DECREF(in_arr);
    return Py_BuildValue("d", result);
}

static struct PyMethodDef methods[] = {
    {"partial_sort", pycoh_partial_sort, METH_VARARGS, partial_sort_docstring},
    {"order_stat", pycoh_order_stat, METH_VARARGS, order_stat_docstring},
    {NULL, NULL, 0, NULL}  /* sentinel */
};

PyMODINIT_FUNC init_order_stats(void) {
    (void) Py_InitModule3("pycoh._order_stats", methods, "fast order statistics\n");
    import_array();
}
