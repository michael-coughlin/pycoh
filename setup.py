import os
import subprocess
import sys
import time
from distutils import log
from distutils.command import build, build_py
from distutils.command import install
from distutils.command import sdist
from distutils.core import setup, Extension

from misc import generate_vcs_info as gvcsi

from numpy.lib.utils import get_include as numpy_get_include

def remove_root(path, root):
  if root:
    return os.path.normpath(path).replace(os.path.normpath(root), "")
  return os.path.normpath(path)

def write_build_info():
    """
    Get VCS info from misc/generate_vcs_info.py and add build information.
    Substitute these into misc/git_version.py.in to produce
    pycoh/git_version.py.
    """
    vcs_info = gvcsi.generate_git_version_info()

    # determine current time and treat it as the build time
    build_date = time.strftime('%Y-%m-%d %H:%M:%S +0000', time.gmtime())

    # determine builder
    retcode, builder_name = gvcsi.call_out(('git', 'config', 'user.name'))
    if retcode:
        builder_name = "Unknown User"
    retcode, builder_email = gvcsi.call_out(('git', 'config', 'user.email'))
    if retcode:
        builder_email = ""
    builder = "%s <%s>" % (builder_name, builder_email)

    sed_cmd = ('sed',
        '-e', 's/@ID@/%s/' % vcs_info.id,
        '-e', 's/@DATE@/%s/' % vcs_info.date,
        '-e', 's/@BRANCH@/%s/' % vcs_info.branch,
        '-e', 's/@TAG@/%s/' % vcs_info.tag,
        '-e', 's/@AUTHOR@/%s/' % vcs_info.author,
        '-e', 's/@COMMITTER@/%s/' % vcs_info.committer,
        '-e', 's/@STATUS@/%s/' % vcs_info.status,
        '-e', 's/@BUILDER@/%s/' % builder,
        '-e', 's/@BUILD_DATE@/%s/' % build_date,
        'misc/git_version.py.in')

    # FIXME: subprocess.check_call becomes available in Python 2.5
    sed_retcode = subprocess.call(sed_cmd,
        stdout=open('pycoh/git_version.py', 'w'))
    if sed_retcode:
        raise gvcsi.GitInvocationError

class pycoh_build(build.build):
    def run(self):
        # If we are building from a release tarball, do not distribute scripts.
        # PKG-INFO is inserted into the tarball by the sdist target.
        if os.path.exists("PKG-INFO"):
            self.distribution.scripts = []

        # resume normal build procedure
        build.build.run(self)

class pycoh_build_py(build_py.build_py):
    def run(self):
        # If we are building from tarball, do not update git version.
        # PKG-INFO is inserted into the tarball by the sdist target.
        if not os.path.exists("PKG-INFO"):
            # create the git_version module
            try:
                write_build_info()
                log.info("Generated pycoh/git_version.py")
            except gvcsi.GitInvocationError:
                if not os.path.exists("pycoh/git_version.py"):
                    log.error("Not in git checkout or cannot find git executable.")
                    sys.exit(1)

        # resume normal build procedure
        build_py.build_py.run(self)

class pycoh_install(install.install):
    def run(self):
        pycoh_prefix = remove_root(self.prefix, self.root)

        # Detect whether we are building from a tarball; we have decided
        # that releases should not contain scripts.
        # PKG-INFO is inserted into the tarball by the sdist target.
        if os.path.exists("PKG-INFO"):
            self.distribution.scripts = []
        # Hardcode a check for system-wide installation;
        # in this case, don't make the user-env scripts.
        if pycoh_prefix == sys.prefix:
            self.distribution.data_files = []
            install.install.run(self)
            return

        # create the user env scripts
        if self.install_purelib == self.install_platlib:
            pycoh_pythonpath = self.install_purelib
        else:
            pycoh_pythonpath = self.install_platlib + ":" + self.install_purelib

        pycoh_install_scripts = remove_root(self.install_scripts, self.root)
        pycoh_pythonpath = remove_root(pycoh_pythonpath, self.root)
        pycoh_install_platlib = remove_root(self.install_platlib, self.root)

        if not os.path.isdir("etc"):
            os.mkdir("etc")
        log.info("creating pycoh-user-env.sh script")
        env_file = open(os.path.join("etc", "pycoh-user-env.sh"), "w")
        print >> env_file, "# Source this file to access PYCOH"
        print >> env_file, "PYCOH_PREFIX=" + pycoh_prefix
        print >> env_file, "export PYCOH_PREFIX"
        if self.distribution.scripts:
            print >> env_file, "PATH=" + pycoh_install_scripts + ":${PATH}"
            print >> env_file, "export PATH"
        print >> env_file, "PYTHONPATH=" + pycoh_pythonpath + ":${PYTHONPATH}"
        print >> env_file, "LD_LIBRARY_PATH=" + pycoh_install_platlib + ":${LD_LIBRARY_PATH}"
        print >> env_file, "DYLD_LIBRARY_PATH=" + pycoh_install_platlib + ":${DYLD_LIBRARY_PATH}"
        print >> env_file, "export PYTHONPATH LD_LIBRARY_PATH DYLD_LIBRARY_PATH"
        print >> env_file, "_CONDOR_DAGMAN_LOG_ON_NFS_IS_ERROR=FALSE"
        print >> env_file, "export _CONDOR_DAGMAN_LOG_ON_NFS_IS_ERROR"
        env_file.close()

        log.info("creating pycoh-user-env.csh script")
        env_file = open(os.path.join("etc", "pycoh-user-env.csh"), "w")
        print >> env_file, "# Source this file to access PYCOH"
        print >> env_file, "setenv PYCOH_PREFIX " + pycoh_prefix
        if self.distribution.scripts:
            print >> env_file, "setenv PATH " + pycoh_install_scripts + ":${PATH}"
        print >> env_file, "if ( $?PYTHONPATH ) then"
        print >> env_file, "  setenv PYTHONPATH " + pycoh_pythonpath + ":${PYTHONPATH}"
        print >> env_file, "else"
        print >> env_file, "  setenv PYTHONPATH " + pycoh_pythonpath
        print >> env_file, "endif"
        print >> env_file, "if ( $?LD_LIBRARY_PATH ) then"
        print >> env_file, "  setenv LD_LIBRARY_PATH " + pycoh_install_platlib + ":${LD_LIBRARY_PATH}"
        print >> env_file, "else"
        print >> env_file, "  setenv LD_LIBRARY_PATH " + pycoh_install_platlib
        print >> env_file, "endif"
        print >> env_file, "if ( $?DYLD_LIBRARY_PATH ) then"
        print >> env_file, "  setenv DYLD_LIBRARY_PATH " + pycoh_install_platlib + ":${DYLD_LIBRARY_PATH}"
        print >> env_file, "else"
        print >> env_file, "  setenv DYLD_LIBRARY_PATH " + pycoh_install_platlib
        print >> env_file, "endif"
        print >> env_file, "setenv _CONDOR_DAGMAN_LOG_ON_NFS_IS_ERROR FALSE"
        env_file.close()

        # now run the installer
        install.install.run(self)


class pycoh_sdist(sdist.sdist):
    def run(self):
        # customize tarball contents
        self.distribution.data_files = []
        for root,dirs,files in os.walk("debian"):
            for file in files:
                self.distribution.data_files += [os.path.join(root,file)]
        self.distribution.data_files += ["pycoh.spec"]
        self.distribution.scripts = []

        # create the git_version module
        try:
            write_build_info()
            log.info("generated pycoh/git_version.py")
        except gvcsi.GitInvocationError:
            if not os.path.exists("pycoh/git_version.py"):
                log.error("Not in git checkout or cannot find git executable. Exiting.")
                sys.exit(1)

        # now run sdist
        sdist.sdist.run(self)


setup(name="pycoh",
      version="0.2",
      description="Python Coherence Calculator",
      author="Nickolas Fotopoulos",
      author_email="nickolas.fotopoulos@ligo.org",
      packages=["pycoh"],
      cmdclass = {
        "build_py": pycoh_build_py,
        "install": pycoh_install,
        "sdist": pycoh_sdist
      },
      scripts=["bin/formPSDCSD", "bin/harvest", "bin/plow", "bin/seed",
        "bin/stochastic_segments", "bin/plotcoherence", "bin/multiply",
        "bin/plottimefreq", "bin/maximize", "bin/identify_outliers"],
      data_files=[("etc", ["etc/pycoh-user-env.sh", "etc/pycoh-user-env.csh"])],
      ext_modules = [
        Extension(
            "pycoh._coarse_grain",
            ["src/_coarse_grain.c"],
            include_dirs = [numpy_get_include()],
            extra_compile_args=["-std=c99", "-march=native"]
        ),
        Extension(
            "pycoh._order_stats",
            ["src/_order_stats.c", "src/_order_stats_wrapper.c"],
            include_dirs = ["include", numpy_get_include()],
            extra_compile_args=["-std=c99", "-march=native"]
        ),
        Extension(
            "pycoh.robust_scale",
            ["src/_order_stats.c", "src/_robust_scale.c", "src/_robust_scale_wrapper.c"],
            include_dirs = ["include", numpy_get_include()],
            extra_compile_args=["-std=c99", "-march=native"]
        )
      ]
)
