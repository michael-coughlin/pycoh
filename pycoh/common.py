"""
Library with miscellaneous, common functions, mostly involving file naming.
"""
from __future__ import division

__author__ = "Nickolas Fotopoulos <nickolas.fotopoulos@ligo.org>"

import os
import socket
import time
import os.path as op
import urlparse

from glue.iterutils import uniq
from glue.lal import CacheEntry, Cache
from glue.segments import segment

def set_nested_dict(basedict, keys, item):
    """
    Assign basedict[keys[0]][keys[1]]..[keys[N-1]] = item,
    creating the sub-dicts as necessary.
    """
    if len(keys) == 0:
        return item
    key = keys[0]
    basedict[key] = set_nested_dict(basedict.get(key, {}), keys[1:], item)
    return basedict

def mkdir_secure(dir, retry=3, exception=None):
    """
    Perform the equivalent of:
    if not os.path.isdir(dir): os.mkdir(dir)
    in such a way that inter-job collision is (mostly) avoided.
    """
    if op.isdir(dir): return
    if retry<=0:
        raise exception

    try:
        os.mkdir(dir)
    except OSError as e:
        time.sleep(2)
        return mkdir_secure(dir, retry-1, e)

def ifos_from_channels(chanlist):
    """
    Determine all ifos represented by a list of channels.
    """
    ifos = list(uniq([chan[:2] for chan in chanlist]))
    ifos.sort()
    return ifos

def channels_from_filename(filename):
    """
    Determine the channels that participated in a given coherence or CSD,
    given the filename.
    """
    filename = op.split(filename)[1]
    chanstring = "-".join(filename.split("-")[1:-2])[4:]
    channels = chanstring.split("+")
    return channels

def cache_filename(cachedir, obs, frametype, chunks):
    """
    Determine the filename of a cachefile.
    """
    extent = chunks.extent()
    start = extent[0]
    end = extent[1]
    dur = end - start
    return "%s/%s-%s_CACHE-%s-%s.lcf" % (cachedir, obs, frametype, start, dur)

def log_filename(logdir, observatories, tag, chunks):
    """
    Determine the filename of a logfile.
    """
    extent = chunks.extent()
    start = extent[0]
    end = extent[1]
    dur = end - start
    obsstr = "".join(observatories)
    return "%s/%s-%s-%s-%s.log" % (logdir, obsstr, tag, start, dur)

def veto_filename(logdir, observatories, tag, chunks):
    """
    Determine the filename of a logfile.
    """
    extent = chunks.extent()
    start = extent[0]
    end = extent[1]
    dur = end - start
    obsstr = "".join(observatories)
    return "%s/%s-%s-%s-%s.xml.gz" % (logdir, obsstr, tag, start, dur)

def fft_filename(fftdir, chanstring, chunks, extension):
    """
    Determine the filename containing a particular set of FFTs.
    """
    ifo = chanstring[:2]
    start, end = chunks.extent()
    dur = end - start
    format = "%s/%s-FFT_%s-%d-%d%s"
    return format % (fftdir, ifo, chanstring, start, dur, extension)

def psd_filename(psddir, chanstring, chunks, extension):
    """
    Determine the filename containing a particular PSD.
    """
    ifo = chanstring[:2]
    start, end = chunks.extent()
    dur = end - start
    format = "%s/%s-PSD_%s-%d-%d%s"
    return format % (psddir, ifo, chanstring, start, dur, extension)

def csd_filename(psddir, chanpair, chunks, extension):
    """
    Determine the filename containing a particular PSD.
    """
    # Want unique set of ifos, in order
    ifos = list(set((chanpair[0][:2], chanpair[1][:2])))
    ifos.sort()
    ifos = "".join(ifos)
    start, end = chunks.extent()
    dur = end - start
    format = "%s/%s-CSD_%s-%d-%d%s"
    return format % (psddir, ifos, '+'.join(chanpair), start, dur, extension)

def x3g3_filename(x3g3dir, chanpair, chunks, extension):
    """
    Determine the filename containing a particular PSD.
    """
    # Want unique set of ifos, in order
    ifos = list(set((chanpair[0][:2], chanpair[1][:2])))
    ifos.sort()
    ifos = "".join(ifos)
    start, end = chunks.extent()
    dur = end - start
    format = "%s/%s-X3GAMMA3_%s-%d-%d%s"
    return format % (x3g3dir, ifos, '+'.join(chanpair), start, dur, extension)

def coh_filename(cohdir, chanpair, chunks, extension):
    """
    Determine the filename containing a particular coherence.
    """
    # Want unique set of ifos, in order
    ifos = list(set((chanpair[0][:2], chanpair[1][:2])))
    ifos.sort()
    ifos = "".join(ifos)
    start, end = chunks.extent()
    dur = end - start
    format = "%s/%s-COH_%s-%d-%d%s"
    return format % (cohdir, ifos, '+'.join(chanpair), start, dur, extension)

class PSDCacheEntry(CacheEntry):
    local_hostname = socket.gethostname()

    @classmethod
    def from_filename(cls, filename):
        """
        Initialize the whole cache entry from just the PSD file name.  Sets
        the hostname to the current machine's hostname.

        Ex:
        >>> e = PSDCacheEntry.from_filename('/tmp/H0-PSD_H0:PEM-BSC1_MAG1X-827449984-62.pickle.gz')
        >>> print str(e)
        'H0 PSD 827449984 62 file://dirac.local/tmp/H0-PSD_H0:PEM-BSC1_MAG1X-827449984-62.pickle.gz'
        """
        filename = op.abspath(filename)
        path, basename = op.split(filename)
        tmp_basename, ext2 = op.splitext(basename)
        naked_basename, ext = op.splitext(tmp_basename)
        ext += ext2
        del ext2

        split_filename = op.split(naked_basename)[1].split("-")
        ifos = split_filename[0]
        tag = "-".join(split_filename[1:-2])
        start = int(split_filename[-2])
        duration = int(split_filename[-1])

        if tag.startswith("PSD_"):
            description = "PSD"
        elif tag.startswith("CSD_"):
            description = "CSD"
        elif tag.startswith("X3GAMMA3_"):
            description = "X3GAMMA3"
        else:
            raise ValueError("Unrecognized description in %s" % filename)
        seg = segment(start, start + duration)
        url = urlparse.urlunsplit(("file", cls.local_hostname, filename, "", ""))

        return cls(ifos, description, seg, url)

    def get_chanstring(self):
        filename = op.split(self.url)[-1]
        tag = "-".join(filename.split('-')[1:-2])
        return tag[tag.find('_')+1:].replace("+", "-")
    chanstring = property(fget=get_chanstring)
    hostname = property(fget=lambda self: self.host())

class PSDCache(Cache):
    entry_class = PSDCacheEntry

    def add_psd(self, psd_filename):
        # modify paths to access nodes' local drives
        if 'node' in self.entry_class.local_hostname: # CIT
          psd_filename = psd_filename.replace("/usr1",
            "/data/%s" % self.entry_class.local_hostname)

        self.append(PSDCacheEntry.from_filename(psd_filename))

    def get_nested_dict(self, *key_types):
        """
        Return nested dictionaries keyed as psd_files[key1][key2]...[keyN] = path,
        where keyi are of the kind key_types[i], and key_type[i] is "observatory",
        "chanstring", "segment", "hostname", or any attribute of PSDFile. Ex:

        >>> l = PSDCache()
        >>> l.add_psd('/tmp/H0-PSD_H0:PEM-BSC1_MAG1X-827449984-62.pickle.gz')
        >>> l.add_psd('/tmp/H0-PSD_H0:PEM-EY_V1-827449984-62.pickle.gz')
        >>> l.get_nested_dict("hostname", "segment", "chanstring")
        {'dirac.local': {segment(827449984, 827450046): {'H0:PEM-BSC1_MAG1X': '/tmp/H0-PSD_H0:PEM-BSC1_MAG1X-827449984-62.pickle.gz',
                                                         'H0:PEM-EY_V1': '/tmp/H0-PSD_H0:PEM-EY_V1-827449984-62.pickle.gz'}}}

        NB: Will silently overwrite paths if your choice of keys do not
        specify a unique path.
        """
        if len(key_types) == 0:
            raise ValueError("Need at least one key to produce dictionary.")
        d = {}
        for pf in self:
            keys = [getattr(pf, key_type) for key_type in key_types]
            set_nested_dict(d, keys, pf.path)
        return d
