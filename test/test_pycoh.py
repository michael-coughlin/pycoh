#!/usr/bin/env python

from __future__ import division
from itertools import izip
import random
import unittest
import numpy as np

from glue.segments import segment, segmentlist

# LDG clusters have an old version of scipy; numpy complains bitterly
import warnings
warnings.filterwarnings("ignore", "^NumpyTest")

# functions to test
from pycoh.signals import coarse_grain
from pycoh.segmentsUtils2 import disjoint_cover

class test_pycoh_signals_coarse_grain_float(unittest.TestCase):
    input_len = 1001
    test_input = np.random.standard_normal(size=(input_len,))
    orig_df = 0.25

    def test_coarse_grain_len(self):
        """
        Verify that coarse_grain returns a vector of the expected length
        """
        # integer ratio downsamples
        for new_df in (0.25, 0.5, 1):
            ratio = int(np.round(new_df / self.orig_df))
            new = coarse_grain(self.test_input, ratio)
            new_len = int(np.ceil(self.input_len / ratio))
            self.assertEqual(len(new), new_len)

    def test_coarse_grain_raises(self):
        """
        Verify that coarse_grain raises the right exceptions on bad data
        """
        # ratio zero or negative
        self.assertRaises(ValueError, coarse_grain, self.test_input, 0)
        self.assertRaises(ValueError, coarse_grain, self.test_input, -15)

    def test_coarse_grain_conserves_power(self):
        """
        Verify that coarse_grain conserves power for integer ratio downsamples
        """
        for new_df in (0.25, 0.5, 1):
            ratio = int(np.round(new_df / self.orig_df))
            new = coarse_grain(self.test_input, ratio)
            self.assertAlmostEqual(new.sum(), self.test_input.sum())

class test_pycoh_signals_coarse_grain_complex(unittest.TestCase):
    input_len = 1001
    test_input = np.random.standard_normal(size=(input_len,)) + 1j*np.random.standard_normal(size=(input_len,))
    orig_df = 0.25

    def test_coarse_grain_len(self):
        """
        Verify that coarse_grain returns a vector of the expected length
        """
        # integer ratio downsamples
        for new_df in (0.25, 0.5, 1, 1.75, 95.25):
            ratio = int(np.round(new_df / self.orig_df))
            new = coarse_grain(self.test_input, ratio)
            new_len = int(np.ceil(self.input_len / ratio))
            self.assertEqual(len(new), new_len)

    def test_coarse_grain_raises(self):
        """
        Verify that coarse_grain raises the right exceptions on bad data
        """
        # ratio zero or negative
        self.assertRaises(ValueError, coarse_grain, self.test_input, 0)
        self.assertRaises(ValueError, coarse_grain, self.test_input, -15)

    def test_coarse_grain_conserves_power(self):
        """
        Verify that coarse_grain conserves power for integer ratio downsamples
        """
        for new_df in (0.25, 0.5, 1, 1.75, 95.25):
            ratio = int(np.round(new_df / self.orig_df))
            new = coarse_grain(self.test_input, ratio)
            self.assertAlmostEqual(new.sum(), self.test_input.sum())

    def test_coarse_grain_real(self):
        """
        Regression test for problems with array views and strides.
        """
        for new_df in (0.25, 0.5, 1, 1.75, 95.25):
            ratio = int(np.round(new_df / self.orig_df))
            self.assertTrue(np.allclose(
                coarse_grain(self.test_input, ratio).real,
                coarse_grain(self.test_input.real, ratio)))

    def test_coarse_grain_imag(self):
        """
        Regression test for problems with array views and strides.
        """
        for new_df in (0.25, 0.5, 1, 1.75, 95.25):
            ratio = int(np.round(new_df / self.orig_df))
            self.assertTrue(np.allclose(
                coarse_grain(self.test_input, ratio).imag,
                coarse_grain(self.test_input.imag, ratio)))

class test_pycoh_disjoint_cover(unittest.TestCase):
    def test_docstring_example(self):
        """
        Verify that the example in the docstring works.
        """
        seglist = segmentlist([segment(1, 3), segment(1, 2), segment(2, 3), segment(4, 5)])
        self.assertEqual(disjoint_cover(seglist), segmentlist([segment(1, 3), segment(4, 5)]))

    def test_optimality_on_broken_segments(self):
        """
        Start with large segments and insert sub-segments. Make sure that
        the large segments are recovered.
        """
        N = 1000
        nums = iter(np.random.randint(100, 1000, size=(N,)).cumsum())
        orig_segments = segmentlist([segment(a, b) for a, b in zip(nums, nums)])

        new_segments = segmentlist(orig_segments[:])
        for start, end in orig_segments:
            num_breaks = np.random.randint(5)
            if num_breaks == 0:
                continue
            breaks = np.random.randint(start + 1, end, size=(num_breaks,))
            breaks.sort()
            new_segments.append(segment(start, breaks[0]))
            new_segments.extend(segment(a, b) for a, b in \
                                izip(breaks, breaks[1:]) if a != b)
            new_segments.append(segment(breaks[-1], end))
        random.shuffle(new_segments)  # np.random.shuffle turns segments to tuples
        self.assertEqual(disjoint_cover(new_segments), orig_segments)

# construct and run the test suite.
suite = unittest.TestSuite()
suite.addTest(unittest.makeSuite(test_pycoh_signals_coarse_grain_float))
suite.addTest(unittest.makeSuite(test_pycoh_signals_coarse_grain_complex))
suite.addTest(unittest.makeSuite(test_pycoh_disjoint_cover))
unittest.TextTestRunner(verbosity=2).run(suite)
