#!/usr/bin/env python
# Copyright (C) 2012  Nickolas Fotopoulos
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

from pycoh._order_stats import partial_sort, order_stat

def lowmed(a):
    """ Low median """
    return order_stat(a, (len(a) + 1) // 2)

def himed(a):
    """ High median """
    return order_stat(a, len(a) // 2 + 1)

def median(a):
    return 0.5 * (lowmed(a) + himed(a))
