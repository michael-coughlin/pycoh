/*
 *  Copyright (C) 2005--2007  Martin Maechler, ETH Zurich
 *  Copyright (C) 2012  Nickolas Fotopoulos
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* This is a merge of the C version of original files  qn.f and sn.f,
 * translated by f2c (version 20010821).
 * and then by f2c-clean,v 1.9 2000/01/13 13:46:53
 * and further clean-edited manually by Martin Maechler.
 *
 * Further added interface functions to be called via .C() from R or S-plus
 * Note that Peter Rousseeuw has explicitely given permission to
 * use his code under the GPL for the R project.
 *
 * Once GPLed, NF converted it to pure C99 for more general consumption.
 */

/* Original comments by the authors of the Fortran original code,
 * (merged for Qn & Sn in one file by M.M.):

   This file contains fortran functions for two new robust estimators
   of scale denoted as Qn and Sn, decribed in Rousseeuw and Croux (1993).
   These estimators have a high breakdown point and a bounded influence
   function. The implementation given here is very fast (running in
   O(n logn) time) and needs little storage space.

        Rousseeuw, P.J. and Croux, C. (1993)
        Alternatives to the Median Absolute Deviation",
        Journal of the American Statistical Association, Vol. 88, 1273-1283.

   For both estimators, implementations in the pascal language can be
   obtained from the original authors.

   This software may be used and copied freely for scientific
   and/or non-commercial purposes, provided reference is made
   to the abovementioned paper.

Note by MM: We have explicit permission from P.Rousseeuw to
licence it under the GNU Public Licence.
*/

#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "order_stats.h"
#include "robust_scale.h"

/* these have no extra factors (no consistency factor & finite_corr): */
static double sn0(double * const restrict x, const size_t n, const int is_sorted, double *a2);

/* ----------- Local functions -----------------------------------*/

static int double_cmp(const void *ptr_a, const void *ptr_b) {
    double a = *((double *) ptr_a);
    double b = *((double *) ptr_b);
    return (a >= b) - (b >= a);
}


static double sn0(double * const restrict x0, const size_t n, const int is_sorted, double * restrict a2)
{
/*
   Efficient algorithm for the scale estimator:

       S*_n = LOMED_{i} HIMED_{i} |x_i - x_j|

   which can equivalently be written as

       S*_n = LOMED_{i} LOMED_{j != i} |x_i - x_j|

   Arguments :

       x  : double array (length >= n) containing the observations
       n  : number of observations (n>=2)

       is_sorted: logical indicating if x is already sorted
       a2 : workspace to contain a2[i] := LOMED_{j != i} | x_i - x_j |,
            for i=1,...,n
*/
    double *x;
    double medA, medB;

    ssize_t i, diff, half, Amin, Amax, even, length;
    ssize_t leftA,leftB, nA,nB, tryA,tryB, rightA,rightB;
    size_t n1_2;

    if (is_sorted)
        x = x0;
    else {
        x = malloc(n * sizeof(double));
        memcpy(x, x0, n * sizeof(double));
        qsort(x, n, sizeof(*x), double_cmp);
    }

    a2[0] = x[n / 2] - x[0];
    n1_2 = (n + 1) / 2;

    /* first half for() loop : */
    for (i = 2; i <= n1_2; ++i) {
        nA = i - 1;
        nB = n - i;
        diff = nB - nA;
        leftA = leftB = 1;
        rightA = rightB = nB;
        Amin = diff / 2 + 1;
        Amax = diff / 2 + nA;

        while (leftA < rightA) {
            length = rightA - leftA + 1;
            even = 1 - length % 2;
            half = (length - 1) / 2;
            tryA = leftA + half;
            tryB = leftB + half;
            if (tryA < Amin) {
                rightB = tryB;
                leftA = tryA + even;
            }
            else {
                if (tryA > Amax) {
                    rightA = tryA;
                    leftB = tryB + even;
                }
                else {
                    medA = x[i - 1] - x[i - tryA + Amin - 2];
                    medB = x[tryB + i - 1] - x[i - 1];
                    if (medA >= medB) {
                        rightA = tryA;
                        leftB = tryB + even;
                    } else {
                    rightB = tryB;
                    leftA = tryA + even;
                    }
                }
            }
        } /* while */

        if (leftA > Amax) {
            a2[i - 1] = x[leftB + i - 1] - x[i - 1];
        } else {
            medA = x[i - 1] - x[i - leftA + Amin - 2];
            medB = x[leftB + i - 1] - x[i - 1];
            a2[i - 1] = fmin(medA, medB);
        }
    }

    /* second half for() loop : */
    for (i = n1_2 + 1; i <= n - 1; ++i) {
        nA = n - i;
        nB = i - 1;
        diff = nB - nA;
        leftA  = leftB = 1;
        rightA = rightB = nB;
        Amin = diff / 2 + 1;
        Amax = diff / 2 + nA;

        while (leftA < rightA) {
            length = rightA - leftA + 1;
            even = 1 - length % 2;
            half = (length - 1) / 2;
            tryA = leftA + half;
            tryB = leftB + half;
            if (tryA < Amin) {
                rightB = tryB;
                leftA = tryA + even;
            } else {
                if (tryA > Amax) {
                    rightA = tryA;
                    leftB = tryB + even;
                } else {
                    medA = x[i + tryA - Amin] - x[i - 1];
                    medB = x[i - 1] - x[i - tryB - 1];
                    if (medA >= medB) {
                        rightA = tryA;
                        leftB = tryB + even;
                    } else {
                        rightB = tryB;
                        leftA = tryA + even;
                    }
                }
            }
        } /* while */

        if (leftA > Amax) {
            a2[i - 1] = x[i - 1] - x[i - leftB - 1];
        } else {
            medA = x[i + leftA - Amin] - x[i - 1];
            medB = x[i - 1] - x[i - leftB - 1];
            a2[i - 1] = fmin(medA, medB);
        }
    }
    a2[n - 1] = x[n - 1] - x[n1_2 - 1];

    /* clean up if we ended up having to sort input */
    if (!is_sorted) free(x);

    return order_stat(a2, n, n1_2);
} /* sn0 */

double sn(double *x, size_t n, int is_sorted, int finite_corr)
{
/*
   Efficient algorithm for the scale estimator:

       Sn = cn * 1.1926 * LOMED_{i} HIMED_{i} |x_i-x_j|

   which can equivalently be written as

       Sn = cn * 1.1926 * LOMED_{i} LOMED_{j != i} |x_i-x_j|*/

    double *a2 = calloc(n, sizeof(double));
    double r = 1.1926 * /* asymptotic consistency for sigma^2 */
        sn0(x, n, is_sorted, a2);

    free(a2);

    if (!finite_corr) return r;

    double cn = 1.; /* n >= 10 even */
    if (n <= 9) {
        if      (n == 2) cn = 0.743;
        else if (n == 3) cn = 1.851;
        else if (n == 4) cn = 0.954;
        else if (n == 5) cn = 1.351;
        else if (n == 6) cn = 0.993;
        else if (n == 7) cn = 1.198;
        else if (n == 8) cn = 1.005;
        else if (n == 9) cn = 1.131;
    } else if (n % 2 == 1) /* n odd, >= 11 */
        cn = n / (n - 0.9);
    return cn * r;

} /* sn */
