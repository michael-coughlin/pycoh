#!/usr/bin/env python

from __future__ import division

import unittest
import numpy as np

# LDG clusters have an old version of scipy; numpy complains bitterly
import warnings
warnings.filterwarnings("ignore", "^NumpyTest")

# functions to test
from pycoh.robust_scale import sn

# simpler, brute-force implementations of sn
# Ref: Croux and Rousseeuw, "Time-efficient algorithms for two highly robust estimators of scale" (1992)

def order(a, k):
    """
    Return the k-th order parameter of array a. This function unconditionally
    sorts a!!
    """
    a.sort()
    return a[k - 1]  # 0-indexing

def lowmed(a):
    """ Low median """
    return order(a, (len(a) + 1) // 2)

def himed(a):
    """ High median """
    return order(a, len(a) // 2 + 1)

# bias-correction factors
def cn(n):
    if n > 9:
        if n % 2:  # odd
            return n / (n - 0.9)
        else:
            return 1
    else:
        return \
           (np.inf,  # n = 0
            np.inf,  # n = 1
            0.743,   # n = 2
            1.851,
            0.954,
            1.351,
            0.993,
            1.198,
            1.005,
            1.131    # n = 9
            )[n]

def sn_brute(a):
    a2 = np.zeros_like(a)
    for (i,), xi in np.ndenumerate(a):
        a2[i] = himed(abs(xi - a))
    return cn(len(a)) * 1.1926 * lowmed(a2)

class test_pycoh_robust_scale(unittest.TestCase):
    def test_sn_brute(self):
        for test_size in (5, 6, 20, 21, 40, 41):
            x = np.random.randn(test_size)
            self.assertAlmostEqual(sn(x), sn_brute(x))

    def test_sn_brute_outlier(self):
        for test_size in (5, 6, 20, 21, 40, 41):
            x = np.random.randn(test_size)
            # introduce random outlier
            x[np.random.randint(test_size)] = 99
            self.assertAlmostEqual(sn(x), sn_brute(x))

# construct and run the test suite.
if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(test_pycoh_robust_scale))
    unittest.TextTestRunner(verbosity=2).run(suite)
