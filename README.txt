Pycoh README
------------

Written and maintained by Nickolas Fotopoulos (nickolas.fotopoulos@ligo.org).
Now lives here: https://git.ligo.org/michael-coughlin/pycoh
and is maintained by members of the stochastic group.

Pycoh is a reimplementation of Vuk Mandic's tfcoh, which computes the
coherence function between two channels in gwf files.  Pycoh was written to
optimally handle many pairs of channels at once, but is far more CPU- and I/O-
efficient even for a single pair of channels.  Intermediate data products
are stored on nodes' local disks.

Coherence12 = CSD12 / sqrt(PSD1 * PSD2)

There are a few major differences:
-Can process multiple channel pairs at a time
-Minimizes additional I/O load from cohering multiple pairs of channels --
 loads frame once and runs all PSDs and CSDs on it. There should be no repeat
 retrievals.
-Minimizes CPU load from recomputing FFTs to form PSDs and CSDs.  FFTs are
 cached.  This reduces N*(N+1)/2 FFTs to N FFTs.
-Utilizes DAGs for job control -- allows resuming and running concurrent
 pycoh runs (awkward in tfcoh).
-Jobs have become analyzable segments and windows.  Given a set of science
 mode segments (minus DQ flagged times), and a set of integration windows,
 pycoh will determine what PSDs to compute and will average them for each
 window.

Primary executables
-------------------
formPSDCSD    - performs PSD and CSD calculations
harvest       - totals the segment CSDs and combines to form coherence
plow          - deletes intermediate data products from the nodes' local disks
seed          - performs segment arithmetic and writes a Condor DAG

Prerequisites
-------------
* numpy
* scipy
* glue
* pylal
* matplotlib (for plotting)

Installation instructions
-------------------------
1.  git config --global user.name "Albert Einstein"
2.  git config --global user.email albert.einstein@ligo.org
3.  export GIT_SSH=gsissh
4.  git clone git+ssh://albert.einstein@ligo-vcs.phys.uwm.edu/usr/local/git/pycoh.git
5.  python setup.py install --prefix=${PYCOH_PREFIX}

You will need to fill in real values for "Albert Einstein", albert.einstein, and ${PYCOH_PREFIX}.

Instructions for use
--------------------
0.  Each time you wish to run, set up your environment by running "source
    ${PYCOH_PREFIX}/etc/pycoh-user-env.sh", where you should fill in
    ${PYCOH_PREFIX} with the directory where you installed pycoh. If you want
    this permanently in your environment, you can add this command to your
    ~/.bashrc.
1.  Create a directory to hold your analysis.
2.  Create a list of analyzable segments, i.e. all the segments that have
    valid science data.  Use segwizard or LSCsegFind to find your segments of
    interest.  All segment files must be in segwizard format.
3.  (optional) Create a set of integration windows.  That is, a file with
    segments over which you'd like to cohere data channels. Note that in
    addition to these custom integration windows, one can use the daily-,
    weekly-, and monthly-integration options and/or the integrate-all option
    in the parameter file.
2   Create a parameter file to specify all the details of the coherences you
    want calculated.  Consult test/functional_test/test_nocache.ini for the
    various options.
4.  Run seed to set up the workflow and optionally write out a Condor DAG or
    locally executable script.  Use "seed --help" for details. Example:
    seed --ini-file test_nocache.ini --write-script --write-dag --write-analysis-segments --formPSDCSD --harvest --plow --fPC-group-len 200000
5.  Do a local test run of a single job before submitting to the cluster to
    avoid wasting cluster time debugging. You can find immediately runnable
    jobs in the .sh file generated.
6.  Run condor_submit_dag on your DAG.
7.  Your coherence integrations will appear in results/, labeled by
    starttime-duration. At this point, you can use the post-processing
    routines provided, such as plotcoherence or plottimefreq.

Gotchas
-------
* If your LD_LIBRARY_PATH is set to run compiled Matlab, you may experience
  strange errors.  Remove the Matlab library directories from LD_LIBRARY_PATH
  and try again.
* Job environment variables are taken from the local environment at the time
  of job submission.
